from django.contrib import admin
from core.models import CustomUser
from core.forms import CustomUserCreationForm, CustomUserChangeForm

class CustomUserAdmin(admin.ModelAdmin):

    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    list_display = ['username', 'email']


admin.site.register(CustomUser, CustomUserAdmin)
