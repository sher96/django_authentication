from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from core.views import home, SignupView, HomeView
from rest_framework.authtoken import views

urlpatterns = [
    path('admin/', admin.site.urls),
    
    
    #3rd party auth - Django Allauth
    path('accounts/', include('allauth.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
    path('api-token-auth/', views.obtain_auth_token),
    path('home_json/', HomeView.as_view(), name='home_json'),
     
    #Custom Auth
    path('signup/', SignupView.as_view(), name='signup'),
    #Built in Auth
    # path('accounts/', include('django.contrib.auth.urls')), # This is using the django built-in authentication
    path('', home, name='home'),

]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
